use super::{Color, ColorSignature, Variables, Wallpaper};
use std::io::Write;

fn write_int_color(bytes: &mut Vec<u8>, color: Color) {
    let int = i32::from(color.blue)
        | (i32::from(color.green) << 8)
        | (i32::from(color.red) << (8 * 2))
        | (i32::from(color.alpha) << (8 * 3));

    write!(bytes, "{}", int).unwrap()
}

fn write_hex_color(bytes: &mut Vec<u8>, color: Color) {
    write!(
        bytes,
        "#{alpha:0>2x}{red:0>2x}{green:0>2x}{blue:0>2x}",
        alpha = color.alpha,
        red = color.red,
        green = color.green,
        blue = color.blue,
    )
    .unwrap()
}

pub fn theme_to_bytes(
    variables: &Variables,
    wallpaper: Option<&Wallpaper>,
    color_signature: ColorSignature,
) -> Vec<u8> {
    let write_color = match color_signature {
        ColorSignature::Int => write_int_color,
        ColorSignature::Hex => write_hex_color,
    };

    let mut bytes = Vec::new();

    for (variable, color) in variables {
        bytes.extend_from_slice(variable.as_bytes());
        bytes.push(b'=');
        write_color(&mut bytes, *color);
        bytes.push(b'\n');
    }

    if let Some(wallpaper) = wallpaper {
        bytes.extend_from_slice(b"\nWPS\n");
        bytes.extend_from_slice(wallpaper);
        bytes.extend_from_slice(b"\nWPE\n");
    }

    bytes
}
