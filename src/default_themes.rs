//! Provides Telegram's default themes.
//!
//! As of 5.12.0, Telegram has four default themes: _Default_, _Mono_, _Dark_
//! and _Arctic_. The latter three allow setting a custom accent color, so
//! their generator functions require an HSV color used as the accent color.
//! The function will then generate the rest of the theme as if Telegram
//! generated it.
//!
//! Debug builds of Telegram also include the Graphite theme, and this crate
//! provides it under the `graphite-theme` feature. Note that if this theme
//! is removed without ever reaching production builds, this feature may
//! be removed without a major version bump.
//!
//! The default theme no longer provides all variables, but only those
//! you would get when exporting the theme manually. Instead, use
//! [`attheme::VARIABLES`] which is a slice of all variable names.
//!
//! [`attheme::VARIABLES`]: ../constant.VARIABLES.html

#![allow(clippy::unreadable_literal, clippy::too_many_lines)]

mod arctic;
mod dark;
mod default;
mod mono;

pub use {arctic::arctic, dark::dark, default::default, mono::mono};

#[cfg(feature = "graphite-theme")]
mod graphite;
#[cfg(feature = "graphite-theme")]
pub use graphite::graphite;
