//! A crate for working with `.attheme` files. It has full support for the
//! `.attheme` file format. In addition, it provides all default themes,
//! including setting custom accent colors, and a complete list of variables.
//!
//! Should you have a problem or a question, feel free to file an issue on
//! [our GitLab repository][gitlab].
//!
//! [gitlab]: https://gitlab.com/snejugal/attheme-rs

#![deny(
    future_incompatible,
    nonstandard_style,
    missing_docs,
    clippy::all,
    clippy::pedantic,
    clippy::nursery,
    clippy::cargo
)]
#![allow(clippy::multiple_crate_versions)] // can't do much

mod attheme;
pub mod default_themes;
mod fallbacks;
mod parser;
mod serializer;
mod variables;

pub use {self::attheme::Attheme, fallbacks::FALLBACKS, variables::VARIABLES};

use indexmap::IndexMap;
use palette::Srgba;

/// Represents a variable color value.
pub type Color = Srgba<u8>;

/// An `IndexMap` storing variables of the theme.
pub type Variables = IndexMap<String, Color>;

/// A `Vec` of bytes that represents the image wallpaper of the theme.
pub type Wallpaper = Vec<u8>;

/// Determines how to serialize colors. Used by [`Attheme.to_bytes`].
///
/// [`Attheme.to_bytes`]: ./struct.Attheme.html#method.to_bytes
#[derive(Debug, PartialEq, Clone, Copy)]
pub enum ColorSignature {
    /// Represents colors as #aarrggbb.
    ///
    /// `Color::new(0xff, 0xff, 0xff, 0xff)` becomes `#ffffffff`,
    /// `Color::new(0x10, 0x20, 0x30, 0x40)` becomes `#40102030`.
    Hex,
    /// Represents colors as Java Color Integers.
    ///
    /// `Color::new(0xff, 0xff, 0xff, 0xff)` becomes `-1`,
    /// `Color::new(0x10, 0x20, 0x30, 0x40)` becomes `1074798640`. See the
    /// [guide to .attheme's] to learn about this color representation.
    ///
    /// [guide to .attheme's]: telegra.ph/Complete-guide-to-Android-Telegram-theming-12-31#Text-editors
    Int,
}
