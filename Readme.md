# `attheme-rs`

A crate for working with `.attheme` files. It has full support for the
`.attheme` file format. In addition, it provides all default themes,
including setting custom accent colors, and a complete list of variables.

Should you have a problem or a question, feel free to file an issue on
[our GitLab repository][gitlab].

[gitlab]: https://gitlab.com/snejugal/attheme-rs

## Installation

Add this in your `Cargo.toml`:

```toml
[dependencies]
attheme = "0.2"
```

## Examples

### Working with variables

```rust
use std::fs;
use attheme::{Attheme, Color};

let contents = fs::read("path/to.attheme");
let mut theme = Attheme::from_bytes(&contents[..]);

theme.variables.insert("divider".to_string(), Color::new(0, 0, 0, 0x10));

if theme.variables.get("chat_wallpaper".to_string().is_some() {
  println!("The theme has a color wallpaper");
}
```

### Extracting a theme's wallpaper

```rust
use std::fs;
use attheme::Attheme;

let contents = fs::read("path/to.attheme");
let theme = Attheme::from_bytes(&contents[..]);

if let Some(wallpaper) = theme.wallpaper {
  fs::write("path/to/image.jpg", wallpaper);
}
```

## Documentation

See the documentation on [_docs.rs_][documentation].

[documentation]: https://docs.rs/attheme
